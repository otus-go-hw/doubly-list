package list

import (
	"testing"
)

func TestRemove(t *testing.T) {
	ok := 3
	list := NewList()
	list.PushFront(`PushFront#1`)
	list.PushFront(`PushFront#2`)
	list.PushBack(`PushFront#1`)
	r1 := list.PushFront(`PushFront#3`)
	isRemove := list.Remove(r1)
	if !isRemove {
		t.Fatal(`TestRemove: remove error`)
	}
	if list.Len() != ok {
		t.Fatal(`TestRemove: length does not match`)
	}
}

func TestDoubleRemove(t *testing.T) {
	list := NewList()
	item := list.PushFront(`remove`)
	isRemove := list.Remove(item)
	if !isRemove {
		t.Fatal(`TestDoubleRemove: first remove error`)
	}
	doubleRemove := list.Remove(item)
	if doubleRemove {
		t.Fatal(`TestDoubleRemove: second remove error`)
	}
}

func TestLen(t *testing.T) {
	ok := 5
	list := List{}
	list.PushFront(`PushFront#1`)
	list.PushFront(`PushFront#2`)
	list.PushBack(`PushBack#1`)
	list.PushBack(`PushBack#2`)
	list.PushFront(`PushFront#3`)
	if result := list.Len(); result != ok {
		t.Fatalf("length does not match")
	}
}

func TestFirsAndLast(t *testing.T) {
	list := List{}
	list.PushFront(`font`)
	list.PushBack(`last`)
	list.PushFront(`first`)
	if list.First().Value() != `first` {
		t.Fatalf(`error first item`)
	}
	if list.Last().Value() != `last` {
		t.Fatalf(`error last item`)
	}

}

func TestPushFront(t *testing.T) {
	ok := `ok`
	list := List{}
	list.PushFront(`test`)
	list.PushFront(ok)
	if list.First().Value() != ok {
		t.Fatal(`Error PushFront: value does not match`)
	}
}

func TestPushBack(t *testing.T) {
	ok := `ok`
	list := List{}
	list.PushBack(`test`)
	list.PushBack(ok)
	if list.Last().Value() != ok {
		t.Fatal(`Error PushBack: value does not match`)
	}
}
