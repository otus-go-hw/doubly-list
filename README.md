# Домашнее задание: Двусвязный список

```go get -u gitlab.com/otus-go-hw/doubly-list```


Цель: Реализовать двусвязный список: https://en.wikipedia.org/wiki/Doubly_linked_list​
Завести в репозитории отдельный пакет (модуль) для этого ДЗ
Реализовать типы List и Item (см. ниже) и методы у них.
Написать unit-тесты проверяющие работу всех методов.

Ожидаемые типы (псевдокод):
​
```
List // тип контейнер
Len() // длинна списка
First() // первый Item
Last() // последний Item
PushFront(v interface{}) // добавить значение в начало
PushBack(v interface{}) // добавить значение в конец
Remove(i Item) // удалить элемент
​
Item // элемент списка
Value() interface{} // возвращает значение
Next() *Item // следующий Item
Prev() *Item // предыдущий
```