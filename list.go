package list

type List struct {
	first  *Item
	last   *Item
	length int
}

func NewList() *List {
	return &List{}
}

func (l *List) First() *Item {
	return l.first
}

func (l *List) Last() *Item {
	return l.last
}

func (l *List) PushFront(data interface{}) *Item {
	var item *Item
	item = &Item{
		value: data,
		prev:  nil,
		next:  nil,
		list:  l,
	}
	temp := l.first
	item.next = temp
	l.first = item
	if l.length == 0 {
		l.last = l.first
	} else {
		temp.prev = item
	}
	l.length++
	return item
}

func (l *List) Len() int {
	return l.length
}

func (l *List) Remove(item *Item) bool {
	if item.list != l {
		return false
	}
	if item.prev != nil {
		item.prev.next = item.next
	} else {
		l.first = item.next
	}

	if item.next != nil {
		item.next.prev = item.prev
	} else {
		l.last = item.prev
	}
	item.prev = nil
	item.next = nil
	item.list = nil
	l.length--
	return true
}

func (l *List) PushBack(data interface{}) *Item {
	var item *Item
	item = &Item{
		value: data,
		prev:  nil,
		next:  nil,
		list:  l,
	}
	temp := l.last
	item.prev = temp
	l.last = item
	if l.length == 0 {
		l.first = l.last
	} else {
		temp.next = item
	}
	l.length++
	return item
}
