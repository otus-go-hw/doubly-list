package list

type Item struct {
	value interface{}
	prev  *Item
	next  *Item
	list  *List
}

func (i *Item) Prev() *Item {
	return i.prev
}

func (i *Item) Value() interface{} {
	return i.value
}

func (i *Item) Next() *Item {
	return i.next
}
